#!/bin/bash

# Normal Branding
rm /etc/os-release
echo "NAME=\"AbstractOS\"" >> /etc/os-release
echo "ID=\"abstractos\"" >> /etc/os-release
echo "VERSION_ID=\"0.0.1\"" >> /etc/os-release
echo "BUILD_ID=\"$CI_JOB_ID\"" >> /etc/os-release
echo "PRETTY_NAME=\"Abstract OS $ABSTRACT_VERSION\"" >> /etc/os-release
echo "HOME_URL=\"https://abstractos.org\"" >> /etc/os-release
echo "HOME_URL=\"https://bugs.abstractos.org\"" >> /etc/os-release


# Bootloader
sed -i 's/Alpine/Abstract OS/g' /boot/extlinux.conf

# Setup MOTD
rm /etc/motd

echo "" >> /etc/issue
echo "---" >> /etc/issue
echo "Welcome to Abstract OS!" >> /etc/motd
echo "" >> /etc/motd
echo "Abstract OS is a user-friendly linux operating system based upon alpine linux." >> /etc/motd
echo "You can find out more about Abstract OS at the URLs below:" >> /etc/motd
echo "" >> /etc/motd
echo "Abstract OS: https://abstractos.org" >> /etc/motd
echo "Support: https://support.abstractos.org" >> /etc/motd
echo "Documentation: https://docs.abstractos.org" >> /etc/motd
echo "Our GitLab: https://gitlab.com/abstract/" >> /etc/motd
echo "---" >> /etc/issue
echo "" >> /etc/issue

# Setup Issue
rm /etc/issue

echo "Welcome to Abstract OS! $ABSTRACT_VERSION" >> /etc/issue
echo "Kernel \r on an \m (\l)" >> /etc/issue
echo "" >> /etc/issue
