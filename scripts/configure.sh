#!/bin/sh

_step_counter=0
step() {
	_step_counter=$(( _step_counter + 1 ))
	printf '\n\033[1;36m%d) %s\033[0m\n' $_step_counter "$@" >&2  # bold cyan
}


step 'Set up timezone'
setup-timezone -z Europe/Prague

step 'Set up networking'
cat > /etc/network/interfaces <<-EOF
	auto lo
	iface lo inet loopback


EOF
#	auto eth0
#	iface eth0 inet dhcp
ln -s networking /etc/init.d/net.lo
#ln -s networking /etc/init.d/net.eth0

rc-update add networkmanager default

step 'Adjust rc.conf'
sed -Ei \
	-e 's/^[# ](rc_depend_strict)=.*/\1=NO/' \
	-e 's/^[# ](rc_logger)=.*/\1=YES/' \
	-e 's/^[# ](unicode)=.*/\1=YES/' \
	/etc/rc.conf

step 'Enable services'
rc-update add acpid default
rc-update add chronyd default
rc-update add crond default
rc-update add net.eth0 default
rc-update add net.lo boot
rc-update add termencoding boot

#step 'Enable XOrg'
#setup-xorg-base
#Xorg -configure

step 'Setup User Accounts'
sh ./user_setup.sh

step 'Enable Gnome'
rc-update add udev 
rc-update add udev-trigger 
rc-update add udev-settle 
rc-update add gdm
sh ./setup-gdm.sh
rc-service gdm start


step 'Setup Branding'
sh ./branding.sh


