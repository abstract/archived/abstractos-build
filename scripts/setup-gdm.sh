#!/bin/sh

sed -i '/\[daemon\]/a AutomaticLogin=installer' /etc/gdm/custom.conf 
sed -i '/\[daemon\]/a AutomaticLoginEnable=true' /etc/gdm/custom.conf
sed -i 's/#Enable=true/Enable=true/g' /etc/gdm/custom.conf
echo "XDG_SESSION_TYPE=wayland" >> /etc/environment
