#!/bin/bash

mkdir /home/installer
echo "installer:x:1001:1001:AbstractOS Install User:/home/installer:/bin/bash" >> /etc/passwd
echo -e "installer\ninstaller" | passwd installer
echo "installer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/installer
chmod 0440 /etc/sudoers.d/installer
